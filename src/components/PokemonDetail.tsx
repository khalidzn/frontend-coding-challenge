import { useState, useEffect, forwardRef, useImperativeHandle } from "react";
import { Spinner } from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Image from 'react-bootstrap/Image';

import { useSelector } from "react-redux";

interface Props {
  showModal: boolean,
  setShowModal: (show: boolean) => void
}

const PokemonDetail = ({ showModal, setShowModal }: Props) => {
  const { pokemon, isLoading } = useSelector((state: any) => state.pokemonState);
  
  const handleClose = () => setShowModal(false);

  return (
    <>
      {isLoading ? <Spinner animation="grow" /> :
        <Modal size="sm" show={showModal} onHide={handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Pokemon details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <span className="pokemon-name">{pokemon.name}</span>
            <Image src={pokemon.image} roundedCircle />
          </Modal.Body><br />
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>

          </Modal.Footer>
        </Modal>
      }
    </>
  );

};

export default PokemonDetail;