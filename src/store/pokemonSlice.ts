import { createSlice } from '@reduxjs/toolkit';
import {FIRST_CALL} from "./pokemonApi";

interface Pokemon{
    name: string,
    image: string
}

interface PokemonAction {
    payload:{
        next: string,
        results: []
    }
}
interface PokemonsState {
    route: string,
    pokemons: [],
    pokemon: Pokemon,
    isLoading: boolean
}
const initialState = {
    route: FIRST_CALL,
    pokemons: [],
    pokemon:  {},
    isLoading: false
} as PokemonsState

const pokemonSlice = createSlice({
    name: 'pokemons',
    initialState,
    reducers: {
        fetchPokemons: (state, action) => {
            state.route = action.payload;
            state.isLoading = true;
        },
        fetchPokemonsSuccess: (state, action: PokemonAction) => {
            state.pokemons = [...state.pokemons, ...action.payload.results];
            state.route = action.payload.next;
            state.isLoading = false;
        },

        loadPokemon: (state, action) => {
            state.isLoading = true;
        },
        loadPokemonSuccess: (state, action) => {
            state.pokemon= {image: action.payload.sprites.front_default, name:action.payload.name};
            state.isLoading = false;
        }
    }

})

export const {fetchPokemons, fetchPokemonsSuccess, loadPokemon, loadPokemonSuccess} = pokemonSlice.actions;
export default pokemonSlice.reducer;