import {configureStore} from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();
import pokemonReducer from './pokemonSlice';
import pokemonSaga from './pokemonSaga';

const store = configureStore( {
    reducer:{
        pokemonState : pokemonReducer
    },
     middleware: [sagaMiddleware]
})

sagaMiddleware.run(pokemonSaga);

export default store;