import {put, takeLatest} from 'redux-saga/effects';
import {fetchPokemons, fetchPokemonsSuccess, loadPokemon, loadPokemonSuccess} from "./pokemonSlice";

function* fetchPokemonsSaga(action: any): any {
    const response = yield fetch(action.payload)
    const data = yield response.json()
    yield put(fetchPokemonsSuccess(data))
}

function* loadPokemonSaga(action: any): any {
    const response = yield fetch(action.payload)
    const data = yield response.json()
    yield put(loadPokemonSuccess(data))
}


export function* pokemonSaga() {
    yield takeLatest(fetchPokemons.type, fetchPokemonsSaga);
    yield takeLatest(loadPokemon.type,  loadPokemonSaga);
}

export default pokemonSaga;