import { useEffect, useState  } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchPokemons, loadPokemon } from "./store/pokemonSlice";
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner, Navbar, Container} from "react-bootstrap";

import './App.css';

import PokemonDetail from "./components/PokemonDetail";

function App() {
    const dispatch = useDispatch();
    const { pokemons, route } = useSelector((state: any) => state.pokemonState);
    const [showModal, setShowModal] = useState(false);
    
    useEffect(() => {
        dispatch(fetchPokemons(route))
    }, []);

    console.log(pokemons);

    const loadMore = () => {
        dispatch(fetchPokemons(route))
    }

    const onSelectPokemon = (pokemon: string) => {
        dispatch(loadPokemon(pokemon));

        setShowModal(true);
    }

    return <>
    <Container fluid="md">
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand href="#">Pokemons React App</Navbar.Brand>
        </Container>
        </Navbar>
            <InfiniteScroll
                dataLength={pokemons.length}
                next={loadMore}
                hasMore={true}
                loader={<Spinner animation="grow" />}
            >
                <div className="row">
                    {pokemons.map((pokemon: any) => (
                        <div className="col col-md-3" key={pokemon.name}>
                            <div className="card pokemonCard" onClick={() => onSelectPokemon(pokemon.url)}>
                                <div className="card-body">

                                    <h5 className="card-title">{pokemon.name}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{pokemon.url}</h6>
                                </div>
                            </div>
                        </div>
                    ))
                    }
                </div>
            </InfiniteScroll>
            <PokemonDetail showModal={showModal} setShowModal={setShowModal} />
    </Container>
    </>
}

export default App
