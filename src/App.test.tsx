import { render, screen } from '@testing-library/react';

import App from './App';
import { Provider } from 'react-redux';
import PokemonDetail from './components/PokemonDetail';
import configureStore from 'redux-mock-store';
import { FIRST_CALL } from './store/pokemonApi';

describe('App main component', () => {
  const initialState = {
    pokemonsState: {
      route: FIRST_CALL,
      pokemons: [],
      pokemon: {},
      isLoading: false
    }
  };
  const mockStore = configureStore();
  const store = mockStore(initialState);

  it('renders headline of Pokemons React App', () => {
    render(<Provider store={store}>
      <App />
    </Provider>);

    screen.debug();
  });
});